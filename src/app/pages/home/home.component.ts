import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Product } from 'src/app/models/product/product';
import { CartService } from 'src/app/service/cart-service/cart-service';
import { ProductService } from 'src/app/service/product-service/product.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  public products:Array<Product>;

  constructor(private router:Router,
    private productService:ProductService,
    public cartService: CartService){
    }

  ngOnInit(): any {
    return Promise.all([
      this.getProducts()
    ])
    .catch((error)=>{
      console.error(error.message);
    });
  }

  private getProducts(){
    return this.productService.getProducts()
    .then((products)=>{
      this.products = products;
      return products;
    })
  }

  /**
   * addProducto
   */
  public addProducto(productToAdd) {
    this.cartService.addCart(productToAdd);
  }


}
