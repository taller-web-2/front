import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, ParamMap } from '@angular/router';
import { AuthService } from 'src/app/service/auth-service/auth.service';

@Component({
  selector: 'app-validate',
  templateUrl: './validate.component.html',
  styleUrls: ['./validate.component.css']
})
export class ValidateComponent implements OnInit {
  public message: String;

  constructor(
    private authService: AuthService,
    private route: ActivatedRoute,
  ) { }

  ngOnInit(): any {
    this.route.paramMap
      .subscribe((param: ParamMap)=>{ this.validateUser(param.get('tokenValidator'))});
  }

  public validateUser(param){
    return this.authService.validateUser(param)
    .then(() =>{
      this.message = 'Te has validado con exito!';
    })
    .catch((error)=>{
      this.message = 'Ha ocurrido un error. Intentelo más tarde';
      console.error(error.message);
    })
  }
}
