import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/service/auth-service/auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  public user = {
    email: '',
    password: ''
  };
  public error = {
    message: '',
    show: false,
  };

  constructor(
    private authService: AuthService,
    private router: Router,
  ) { }

  ngOnInit(): void {
    window.addEventListener('keyup', (e) => {
      if (e.key === 'Enter') {
        this.login();
      }
    });
  }

  public login() {
    this.error.show = false;
    this.error.message = '';
    this.authService.login(this.user)
      .then((data) => {
        if (data) {
          return this.router.navigate(['/']);
        }
        this.error.message = 'Usuario incorrecto';
        this.authService.logout();
        return;
      })
      .catch((err) => {
        this.error.show = true;
        this.error.message = err.error.message;
      });
   }

}
