import { Subscription } from 'rxjs/Subscription';
import { Component, ElementRef, HostListener, Input, OnInit } from '@angular/core';
import { Product } from 'src/app/models/product/product';
import { CartService } from '../../service/cart-service/cart-service';

@Component({
  selector: 'app-cart',
  templateUrl: './cart.component.html',
  styles: ['./cart.component.css']
})
export class CartComponent implements OnInit {

  public carrito: Array<Product> = [];
  public subscription: Subscription;
  public total: number;
  public cartEmpty : boolean;
  public checkoutOk: boolean;


  constructor(private cartService: CartService) {
   }

  ngOnInit() {
    this.cartEmpty = true;
    this.checkoutOk = false;
    this.cartService.getCarrito().subscribe(data => {
      this.carrito = data;
      this.total = this.cartService.getTotal();
      if (data.length > 0){
        this.cartEmpty = false;
      }
    },
      error => alert(error));
  }

  deleteProduct(product: Product) {
    this.cartService.deleteCarrito(product);
  }

  checkout(){
    this.checkoutOk = true;
    this.cartService.checkout();

  } 

}
