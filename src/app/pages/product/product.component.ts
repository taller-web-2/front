import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, Validators } from '@angular/forms';
import { ProductService } from 'src/app/service/product-service/product.service';

@Component({
  selector: 'app-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.css']
})
export class ProductComponent implements OnInit {

  productForm;
  submitted = false;

  constructor(    
    private productService: ProductService,
    private formBuilder: FormBuilder,
  ) {
    this.productForm = this.formBuilder.group({
      name: new FormControl('', Validators.required),
      description: new FormControl('', Validators.required),
      classification: new FormControl('', Validators.required),
      price: new FormControl('', [Validators.required]),
    },
    )
  }

  public productOk = {
    message: '',
    show: false,
  };
  public error = {
    message: '',
    show: false,
  };

  get f() { return this.productForm.controls; }

  ngOnInit(): void {

  }

  public newProduct(){
    this.submitted = true;
    this.productOk.show = false;
    this.error.show = false;
    if (this.productForm.valid) {
      console.log(this.productForm.value)
      this.productService.newProduct(this.productForm.value)
        .then(() => {
          this.productOk.show = true;
          this.productOk.message = 'Producto creado correctamente';
          this.productForm.reset();
        })
        .catch((err) => {
          console.log(err);
          this.error.show = true;
          this.error.message = err.error.error;
        });
      return;
    }
  }

}

