import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Product } from 'src/app/models/product/product';
import { CartService } from 'src/app/service/cart-service/cart-service';
import { ProductService } from 'src/app/service/product-service/product.service';

@Component({
  selector: 'app-detail',
  templateUrl: './detail.component.html',
  styleUrls: ['./detail.component.css']
})
export class DetailComponent implements OnInit {

  // public products:Array<Product>;
  public product: Product;
  public id;

  constructor(private router:Router,
    private productService:ProductService,
    public cartService: CartService,
    private route: ActivatedRoute){
    }

  ngOnInit(): any {
    this.id = this.route.snapshot.params['id'];
    return Promise.all([
      this.getProduct(this.id)
    ])
    .catch((error)=>{
      console.error(error.message);
    });
  }

  private getProduct(id){
    return this.productService.getProduct(id)
    .then((product)=>{
      this.product = product;
    });
  }

  public addProducto(product) {
   this.cartService.addCart(product);
 }
}
