import { Component, OnInit } from '@angular/core';
import { FormControl, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/service/auth-service/auth.service';
import { CustomvalidationService } from './validations/validations.component';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css']
})
export class SignupComponent implements OnInit {
  signupForm;
  submitted = false;

  constructor(
    private authService: AuthService,
    private router: Router,
    private formBuilder: FormBuilder,
    private customValidator: CustomvalidationService
  ) {
    this.signupForm = this.formBuilder.group({
      firstName: new FormControl('', Validators.required),
      lastName: new FormControl('', Validators.required),
      address: new FormControl('', Validators.required),
      email: new FormControl('', [Validators.required, Validators.email]),
      password: new FormControl('',  Validators.compose([Validators.required, this.customValidator.patternValidator()])),
      confirmPassword: new FormControl('', Validators.required),
    },
    {
      validator: this.customValidator.MatchPassword('password', 'confirmPassword'),
    })
   }
   public userOk = {
    message: '',
    show: false,
  };
  public error = {
    message: '',
    show: false,
  };

  ngOnInit(): void {
    window.addEventListener('keyup', (e) => {
      if (e.key === 'Enter') {
        this.onSubmit();
      }
    });
  }

  get f() { return this.signupForm.controls; }

  onSubmit(): void {
    this.submitted = true;
    this.userOk.show = false;
    this.error.show = false;
    if (this.signupForm.valid) {
      this.authService.signup(this.signupForm.value)
        .then((data) => {
          this.userOk.show = true;
          this.signupForm.reset();
          this.userOk.message = 'Usuario creado correctamente. Valide su cuenta ingresando a su correo';
        })
        .catch((err) => {
          console.log(err);
          this.error.show = true;
          this.error.message = err.error.error;
        });
      return;
    }
  };

}
