import { Component, OnInit } from '@angular/core';
import { CartService } from '../../service/cart-service/cart-service';
import { Product } from 'src/app/models/product/product';
import { Subscription } from 'rxjs';
import { AuthService } from 'src/app/service/auth-service/auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {
  public carrito: Array<Product> = [];
  public subscription: Subscription;
  public total: number;
  public isitLogged = this.authService.isAuthenticated();

  constructor(private cartService: CartService,
    public authService: AuthService,
    public router: Router) { }

  ngOnInit(): void {
    this.cartService.getCarrito().subscribe(data => {
      this.carrito = data;
      this.total = this.cartService.getTotal();
    },
      error => alert(error));
  }

  public logout() {
    this.authService.logout();
    return this.router.navigate([''])
      .then(() => {
        window.location.reload();
      })
  }


}
