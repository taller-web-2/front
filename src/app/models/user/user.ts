export class User {
  public static fromJson(data: any) {
    return new this(
      data._id,
      data.email,
      data.firstName,
      data.surName,
      data.password,
      data.address,
    );
  }

  constructor(
    public id?: string,
    public email?: string,
    public firstName?: string,
    public surName?: string,
    public password?: string,
    public address?: string,
  ) { }

  public static initialize() {
    return new this(
      '',
      '',
      '',
      '',
      '',
      ''
    );
  };
}
