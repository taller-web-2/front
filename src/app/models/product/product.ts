export class Product {
  public static fromJson(data: any) {
    return new this(
      data._id,
      data.name,
      data.description,
      data.clasification,
      data.price
    );
  }

  constructor(
    public id?: string,
    public name?: string,
    public description?: string,
    public clasification?: string,
    public price?: number
  ) { }

  public static initialize() {
    return new this(
      '',
      '',
      '',
      '',
      0,
    );
  };
}