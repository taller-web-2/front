import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CartComponent } from './pages/cart/cart.component';
import { DetailComponent } from './pages/detail/detail.component';
import { HomeComponent } from './pages/home/home.component';
import { LoginComponent } from './pages/login/login.component';
import { ValidateComponent } from './pages/validate/validate.component';
import { SignupComponent } from './pages/signup/signup.component';
import { ProductComponent } from './pages/product/product.component';
import { NewProductGuard } from './service/guard/new-product.guard';


const routes: Routes = [{
  path: '',
  component: HomeComponent
},
{
  path: 'login',
  component: LoginComponent
},
{
  path: 'validate/:tokenValidator',
  component: ValidateComponent
},
{
  path: 'signup',
  component: SignupComponent
},
{
  path: 'cart',
  component: CartComponent,
},
{
  path: 'detail/:id',
  component: DetailComponent,
},
{
  path: 'newProduct',
  component: ProductComponent,
  canActivate: [NewProductGuard],
}
]

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
