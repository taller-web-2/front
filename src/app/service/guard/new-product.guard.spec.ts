import { TestBed } from '@angular/core/testing';

import { NewProductGuard } from './new-product.guard';

describe('NewProductGuard', () => {
  let guard: NewProductGuard;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    guard = TestBed.inject(NewProductGuard);
  });

  it('should be created', () => {
    expect(guard).toBeTruthy();
  });
});
