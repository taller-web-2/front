import { Injectable } from '@angular/core';
import { Subject } from 'rxjs/Subject';
import * as Rx from 'rxjs/Rx';
import { Product } from 'src/app/models/product/product';
import { Observable, ReplaySubject, BehaviorSubject } from 'rxjs';

@Injectable()
export class CartService {

  private subject: BehaviorSubject<Product[]> = new BehaviorSubject([]);
  private itemsCarrito: Product[] = [];
  private sidebars = [];

  constructor() {
    this.subject.subscribe(data => this.itemsCarrito = data);
  }

  /**
   * addCarrito
   * @param producto
   */
  addCart(producto: Product) {
    this.subject.next([...this.itemsCarrito, producto]);
  }

  /**
   * getCarrito
   */
  getCarrito(): Observable<Product[]> {
    return this.subject;
  }

  /**
   * clearCarrito
   */
   clearCarrito() {
    this.subject.next(null);
  }

  /**deleteCarrito */
  deleteCarrito(producto: Product) {
    const eliminar = this.itemsCarrito.findIndex(p => p.id === producto); 
   this.itemsCarrito.splice(eliminar, 1);
   this.subject.next(this.itemsCarrito);
  }

  /**vaciarCarrito post Checkout */
  checkout(){
    this.itemsCarrito.splice(0, this.itemsCarrito.length);
  } 

  /**
   * getTotal
   */
  getTotal() {
    return this.itemsCarrito.reduce((total, producto: Product) => { return total + producto.price; }, 0);
  }

  
}
