import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { JwtHelperService } from '@auth0/angular-jwt';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(
    private http: HttpClient,
    private jwtHelper: JwtHelperService,
  ) { }

  public login(data) {
    return this.http.post(environment.API_URL + 'auth/login', data)
      .toPromise()
      .then((resp: any) => {
          localStorage.setItem('token', resp.token);
          return resp;
      });
  }

  public validateUser(token){
    return this.http.post(`${environment.API_URL}auth/validateUser/${token}`, {})
      .toPromise()
      .then((resp: any) => {
        return resp;
      });
  }

  public decodeMyToken(): any {
    const token = this.getToken();
    if (!token) {
      throw new Error('empty token');
    }
    return this.jwtHelper.decodeToken(token);
  }

  public logout(): any {
    localStorage.clear();
    localStorage.removeItem('token');
    return localStorage.clear;
  }

  public getToken(): any {
    const token = localStorage.getItem('token');
    if (!token) {
      return null;
    }
    return token;
  }

  public isAuthenticated(): boolean {
    const token = this.getToken();
    if (!token || this.jwtHelper.isTokenExpired(token)) {
      this.logout();
      return false;
    }
    return true;
  }

  public signup(data) {
    return this.http.post(environment.API_URL + 'auth/signup', data)
      .toPromise()
      .then((resp: any) => {
        return resp;
      });
  }

}
