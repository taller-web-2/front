import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Product } from 'src/app/models/product/product';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ProductService {

  constructor(private http: HttpClient) {}

  public getProducts(): Promise<Array<Product>>{

    return this.http.get(`${environment.API_URL}products`)
    .toPromise()
    .then((data:Array<any>) => {
      return data.map((product)=> {
        return Product.fromJson(product);
      })
    })
  }

  public newProduct(product){
    return this.http.post(`${environment.API_URL}products/newProduct`, product)
    .toPromise()
    .then((resp: any) => {
      return resp;
    });
  }
    
  public getProduct(id): Promise<Product>{
    return this.http.get(`${environment.API_URL}products/${id}`)
    .toPromise()
    .then((data) => {
        return Product.fromJson(data);
    })
  }

}
